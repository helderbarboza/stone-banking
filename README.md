# Stone Banking
## Overview

A banking API that allows users to perform financial transactions.

This application is also available at: [https://frugal-composed-goose.gigalixirapp.com](https://frugal-composed-goose.gigalixirapp.com).

## Tech Stack

  - Docker
  - Docker compose
  - Elixir
  - Phoenix
  - Postgresql
  - Ecto

Also some important packages:

  - [credo](https://hexdocs.pm/credo) for static code analysis
  - [excoveralls](https://hexdocs.pm/excoveralls) for test coverage reports

## Prerequisites

To run this application you must have these resources installed:

- [Docker](https://docs.docker.com/engine/install/ubuntu/)
- [Docker Compose](https://docs.docker.com/compose/install/)


## Getting Started

Clone this repository

```bash
$ git clone https://bitbucket.org/helderbarboza/stone-banking.git
$ cd stone-banking
```

### Development with Docker


```bash
# Docker compose setup
$ docker-compose up --build -d

# Setup the database
$ docker-compose exec web mix ecto.setup

# Run the server
$ docker-compose exec web mix phx.server
```

The project will be available at [localhost:4000](http://localhost:4000).

### Testing with Docker

```bash
# Docker compose setup
$ docker-compose up --build -d

# Setup the database
$ docker-compose exec web MIX_ENV=test mix ecto.reset

# Run the tests
$ docker-compose exec web mix test
```

### Development using Visual Studio Code Remote - Container

For better convenience, this repository also has [Remote - Container](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) config files. 

Simply execute the command **Remote-Containers: Reopen in container** on your VSCode.

## API routes

| Path                  | Method | Description                            | authentication? | staff? |
| --------------------- | ------ | -------------------------------------- | :-------------: | :----: |
| /auth                 | GET    | Gets user data from token              |        ✅        |        |
| /auth/register        | POST   | Registers a new user                   |                 |        |
| /auth/login           | POST   | Login to obtain a authentication token |                 |        |
| /auth/logout          | POST   | Deletes user session token             |        ✅        |        |
| /transaction/withdraw | POST   | Perform a withdraw                     |        ✅        |        |
| /transaction/transfer | POST   | Perform a transfer between users       |        ✅        |        |
| /transaction/balance  | GET    | Retrieves user's balance               |        ✅        |        |
| /transaction          | GET    | Lists all transactions                 |        ✅        |   ✅    |
| /transaction/:id      | GET    | Get a transation                       |        ✅        |   ✅    |