# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Stone.Repo.insert!(%Stone.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Stone.Repo.insert!(%Stone.Users.User{
  is_active: true,
  is_staff: true,
  email: "admin@admin.com",
  password: "admin",
  hashed_password: "$2b$12$uWllHE6wEVvraUA2ZcxmRuLphN8f.9wnQzY9jws.AknVSRXeBsmBm",
  full_name: "admin",
  username: "admin"
})
