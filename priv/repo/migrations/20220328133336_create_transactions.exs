defmodule Stone.Repo.Migrations.CreateTransactions do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :amount, :numeric, precision: 11, scale: 2, null: false
      add :description, :string
      add :type, :string
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:transactions, [:user_id])
  end
end
