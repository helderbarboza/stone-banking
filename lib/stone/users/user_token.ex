defmodule Stone.Users.UserToken do
  @moduledoc """
  The `users_tokens` table.
  """

  use TypedEctoSchema
  import Ecto.Query
  alias Stone.Users.User
  alias Stone.Users.UserToken

  @rand_size 32
  @session_validity_in_days 60

  typed_schema "users_tokens" do
    field :token, :binary
    field :context, :string
    belongs_to :user, User

    timestamps updated_at: false
  end

  @doc """
  Generates a token that will be stored in a signed place,
  such as session or cookie. As they are signed, those
  tokens do not need to be hashed.
  """
  @spec build_session_token(User.t()) :: {binary, UserToken.t()}
  def build_session_token(user) do
    token =
      @rand_size
      |> :crypto.strong_rand_bytes()
      |> Base.url_encode64(padding: false)

    {token, %UserToken{token: token, context: "session", user_id: user.id}}
  end

  @doc """
  Checks if the token is valid and returns its underlying lookup query.

  The query returns the user found by the token.
  """
  @spec verify_session_token_query(binary) :: {:ok, Ecto.Query.t()}
  def verify_session_token_query(token) do
    query =
      from token in token_and_context_query(token, "session"),
        join: user in assoc(token, :user),
        where: token.inserted_at > ago(@session_validity_in_days, "day"),
        select: user

    {:ok, query}
  end

  @doc """
  Returns the given token with the given context.
  """
  @spec token_and_context_query(binary, String.t()) :: Ecto.Query.t()
  def token_and_context_query(token, context) do
    from UserToken, where: [token: ^token, context: ^context]
  end
end
