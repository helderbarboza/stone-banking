defmodule Stone.Users.UserNotifier do
  @moduledoc """
  Email API.
  """

  use Phoenix.Swoosh,
    view: StoneWeb.EmailView,
    layout: {StoneWeb.LayoutView, :email}

  alias Stone.Mailer

  @doc "Deliver transaction details."
  def deliver_transaction_notification(user, transaction) do
    new()
    |> from("no-reply@stone.com")
    |> to(user.email)
    |> subject("Transaction done!")
    |> render_body("transaction.html", %{user: user, transaction: transaction})
    |> Mailer.deliver()
  end
end
