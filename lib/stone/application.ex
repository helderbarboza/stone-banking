defmodule Stone.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Stone.Repo,
      StoneWeb.Telemetry,
      {Phoenix.PubSub, name: Stone.PubSub},
      StoneWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: Stone.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def config_change(changed, _new, removed) do
    StoneWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
