defmodule Stone.Repo do
  @moduledoc false
  use Ecto.Repo,
    otp_app: :stone,
    adapter: Ecto.Adapters.Postgres
end
