defmodule Stone.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Stone.Repo

  alias Stone.Users.User
  alias Stone.Users.UserToken

  @doc "Gets a single user."
  @spec get_user!(any) :: any
  def get_user!(id), do: Repo.get!(User, id)

  ## User registration

  @doc "Registers a user."
  @spec register_user(map) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def register_user(attrs) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  ## Session

  @doc "Generates a session token."
  @spec generate_user_session_token(User.t()) :: binary
  def generate_user_session_token(user) do
    {token, user_token} = UserToken.build_session_token(user)
    Repo.insert!(user_token)

    token
  end

  @doc "Gets the user with the given signed token."
  @spec get_user_by_session_token(binary) :: UserToken.t() | nil
  def get_user_by_session_token(token) do
    {:ok, query} = UserToken.verify_session_token_query(token)

    Repo.one(query)
  end

  @doc "Gets a user by email and password."
  @spec get_user_by_email_and_password(binary, binary) :: User.t() | nil
  def get_user_by_email_and_password(email, password)
      when is_binary(email) and is_binary(password) do
    user = Repo.get_by(User, email: email)
    if User.valid_password?(user, password), do: user
  end

  @doc "Deletes the signed session token."
  @spec delete_session_token(binary) :: :ok
  def delete_session_token(token) do
    token
    |> UserToken.token_and_context_query("session")
    |> Repo.delete_all()

    :ok
  end
end
