defmodule Stone.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  import String, only: [to_integer: 1]

  alias Stone.Accounts.Transaction
  alias Stone.Repo
  alias Stone.Users.User

  @doc "Returns the list of transactions."
  @spec list_transactions :: [Transaction.t()]
  def list_transactions, do: Repo.all(Transaction)

  @doc "Returns the list of transactions by year."
  @spec list_transactions_by_year(binary) :: [Transaction.t()]
  def list_transactions_by_year(year) do
    Transaction
    |> where(fragment("date_part('year', inserted_at)") == ^to_integer(year))
    |> Repo.all()
  end

  @doc "Returns the list of transactions by month."
  @spec list_transactions_by_month(binary, binary) :: [Transaction.t()]
  def list_transactions_by_month(year, month) do
    Transaction
    |> where(fragment("date_part('year', inserted_at)") == ^to_integer(year))
    |> where(fragment("date_part('month', inserted_at)") == ^to_integer(month))
    |> Repo.all()
  end

  @doc "Returns the list of transactions by day."
  @spec list_transactions_by_day(binary, binary, binary) :: [Transaction.t()]
  def list_transactions_by_day(year, month, day) do
    Transaction
    |> where(fragment("date_part('year', inserted_at)") == ^to_integer(year))
    |> where(fragment("date_part('month', inserted_at)") == ^to_integer(month))
    |> where(fragment("date_part('day', inserted_at)") == ^to_integer(day))
    |> Repo.all()
  end

  @doc "Gets a single transaction."
  @spec get_transaction!(any) :: Transaction.t()
  def get_transaction!(id) do
    Repo.get!(Transaction, id)
  end

  @doc "Creates a transaction."
  @spec create_transaction(map) :: {:ok, Transaction.t()} | {:error, Ecto.Changeset.t()}
  def create_transaction(attrs \\ %{}) do
    %Transaction{}
    |> Transaction.changeset(attrs)
    |> Repo.insert()
  end

  @doc "Updates a transaction."
  @spec update_transaction(Transaction.t(), map) ::
          {:ok, Transaction.t()} | {:error, Ecto.Changeset.t()}
  def update_transaction(%Transaction{} = transaction, attrs) do
    transaction
    |> Transaction.changeset(attrs)
    |> Repo.update()
  end

  @doc "Deletes a transaction."
  @spec delete_transaction(Transaction.t()) ::
          {:ok, Transaction.t()} | {:error, Ecto.Changeset.t()}
  def delete_transaction(%Transaction{} = transaction) do
    Repo.delete(transaction)
  end

  ## Operations

  @doc "Create a withdraw transaction."
  @spec create_withdraw(User.t(), binary) ::
          {:ok, Transaction.t()} | {:error, Ecto.Changeset.t()}
  def create_withdraw(user, amount) do
    amount = parse_money(amount)

    attrs = %{
      description: "Withdrawn R$ #{amount} from #{user.username}",
      type: "withdraw",
      amount: Decimal.negate(amount),
      user_id: user.id
    }

    attrs
    |> Transaction.debit_changeset()
    |> Repo.insert()
  end

  @doc "Transfer money between two users."
  @spec create_transfer(User.t(), User.t(), binary) ::
          {:error, Ecto.Changeset.t()} | {:ok, Transaction.t(), Transaction.t()}
  def create_transfer(payer, receiver, amount) do
    amount = parse_money(amount)

    attrs = %{
      description: "Transfered R$ #{amount} from #{payer.username} to #{receiver.username}",
      type: "transfer",
      amount: nil,
      user_id: nil
    }

    attrs_payer = %{attrs | amount: Decimal.negate(amount), user_id: payer.id}
    attrs_receiver = %{attrs | amount: amount, user_id: receiver.id}

    transaction_result =
      Ecto.Multi.new()
      |> Ecto.Multi.insert(:debit, Transaction.debit_changeset(attrs_payer))
      |> Ecto.Multi.insert(:credit, Transaction.credit_changeset(attrs_receiver))
      |> Repo.transaction()

    case transaction_result do
      {:ok, %{debit: transaction_debit, credit: transaction_credit}} ->
        {:ok, transaction_debit, transaction_credit}

      {:error, :debit, changeset, _changes_so_far} ->
        {:error, changeset}

      {:error, :credit, changeset, _changes_so_far} ->
        {:error, changeset}
    end
  end

  @doc "Gets the balance from given user_id."
  @spec user_balance(User.t()) :: Decimal.t()
  def user_balance(user) do
    Transaction
    |> where(user_id: ^user.id)
    |> select([q], coalesce(sum(q.amount), "0"))
    |> Repo.one!()
  end

  @doc "Adds $1000 to user's balance."
  @spec give_new_user_bonus(User.t()) :: {:ok, Transaction.t()} | {:error, Ecto.Changeset.t()}
  def give_new_user_bonus(user) do
    %{
      user_id: user.id,
      amount: "1000",
      type: "bonus",
      description: "Welcome!"
    }
    |> Transaction.credit_changeset()
    |> Repo.insert()
  end

  defp parse_money(value) when is_binary(value) do
    if value =~ ~r'^[+-]?(\d+|\d+\.\d*|\d*\.\d+)$' do
      value
      |> Decimal.new()
      |> Decimal.round(2, :down)
    else
      raise RuntimeError, "invalid format: #{inspect(value)}"
    end
  end
end
