defmodule Stone.Accounts.Transaction do
  @moduledoc """
  The `transactions` table.
  """

  use TypedEctoSchema
  import Ecto.Changeset

  @typep data :: Ecto.Schema.t() | Ecto.Changeset.t()

  typed_schema "transactions" do
    field :amount, :decimal, null: false
    field :description, :string
    field :type, Ecto.Enum, values: ~w[deposit withdraw transfer bonus payment]a
    belongs_to :user, Stone.Users.User

    timestamps()
  end

  @doc "Changeset for any operation."
  @spec changeset(data, map) :: Ecto.Changeset.t()
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:amount, :description, :type, :user_id])
    |> validate_required([:amount, :description, :type, :user_id])
    |> foreign_key_constraint(:user_id)
  end

  @doc "Changeset for debit operations"
  @spec debit_changeset(data, map) :: Ecto.Changeset.t()
  def debit_changeset(transaction \\ %__MODULE__{}, attrs) do
    transaction
    |> changeset(attrs)
    |> validate_number(:amount, less_than: 0)
  end

  @doc "Changeset for credit operations"
  @spec credit_changeset(data, map) :: Ecto.Changeset.t()
  def credit_changeset(transaction \\ %__MODULE__{}, attrs) do
    transaction
    |> changeset(attrs)
    |> validate_number(:amount, greater_than: 0)
  end
end
