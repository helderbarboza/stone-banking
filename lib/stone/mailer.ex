defmodule Stone.Mailer do
  @moduledoc """
  Mailer adapter.
  """
  use Swoosh.Mailer, otp_app: :stone
end
