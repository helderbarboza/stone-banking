defmodule StoneWeb.Transaction do
  @moduledoc "Transaction context."
  alias Stone.Accounts
  alias Stone.Repo
  alias Stone.Users.User

  @doc "Checks if user's current balance cover the given amount."
  @spec validate_funds(User.t(), Decimal.decimal()) :: :ok | {:error, :not_enough_funds}
  def validate_funds(user, amount) do
    balance = Accounts.user_balance(user)

    if Decimal.compare(balance, amount) in [:eq, :gt],
      do: :ok,
      else: {:error, :not_enough_funds}
  end

  @doc "Checks if the given users are different."
  @spec validate_different_users(any, any) :: :ok | {:error, :same_user}
  def validate_different_users(user, user), do: {:error, :same_user}
  def validate_different_users(_a, _b), do: :ok

  @doc "Checks if the given user is active and exists."
  @spec validate_user(any) :: {:ok, User.t()} | {:error, :inactive_user | :user_not_found}
  def validate_user(user_id) do
    if user = Repo.get(User, user_id) do
      if user.is_active, do: {:ok, user}, else: {:error, :inactive_user}
    else
      {:error, :user_not_found}
    end
  end
end
