defmodule StoneWeb.Auth do
  @moduledoc """
  Auth handling.
  """

  import Plug.Conn
  alias Stone.Users
  alias Stone.Users.User

  @doc "Generates a session token from user data."
  @spec get_token(User.t()) :: binary
  def get_token(user), do: Users.generate_user_session_token(user)

  @doc "Deletes a given session token."
  @spec delete_token(binary) :: :ok
  def delete_token(token), do: Users.delete_session_token(token)

  @doc "Used for routes that require the user to not be authenticated."
  @spec require_guest_user(Plug.Conn.t(), any) :: Plug.Conn.t()
  def require_guest_user(conn, _opts) do
    if conn.assigns[:current_user] do
      conn
      |> put_status(401)
      |> Phoenix.Controller.put_view(StoneWeb.ErrorView)
      |> Phoenix.Controller.render(:"401")
      |> halt()
    else
      conn
    end
  end

  @doc "Used for routes that require the user to be authenticated."
  @spec require_authenticated_user(Plug.Conn.t(), any) :: Plug.Conn.t()
  def require_authenticated_user(conn, _opts) do
    if conn.assigns[:current_user] != nil and conn.assigns[:current_user].is_active do
      conn
    else
      conn
      |> put_status(401)
      |> Phoenix.Controller.put_view(StoneWeb.ErrorView)
      |> Phoenix.Controller.render(:"401")
      |> halt()
    end
  end

  @doc "Used for routes that require the user to be authenticated and staff."
  @spec require_staff_user(Plug.Conn.t(), any) :: Plug.Conn.t()
  def require_staff_user(conn, _opts) do
    if conn.assigns[:current_user] != nil and conn.assigns[:current_user].is_staff do
      conn
    else
      conn
      |> put_status(401)
      |> Phoenix.Controller.put_view(StoneWeb.ErrorView)
      |> Phoenix.Controller.render(:"401")
      |> halt()
    end
  end

  @doc "Authenticates the user by looking into the session and assigns the `:current_user`."
  @spec fetch_current_user(Plug.Conn.t(), any) :: Plug.Conn.t()
  def fetch_current_user(conn, _opts) do
    token =
      conn
      |> get_req_header("authorization")
      |> fetch_token()

    user = token && Users.get_user_by_session_token(token)
    assign(conn, :current_user, user)
  end

  defp fetch_token([]), do: nil
  defp fetch_token(["Token " <> token | _tail]), do: String.trim(token)
end
