defmodule StoneWeb.AuthController do
  use StoneWeb, :controller

  import StoneWeb.Auth
  alias Stone.Users
  alias Stone.Accounts

  action_fallback StoneWeb.FallbackController

  plug :require_authenticated_user when action in [:index, :logout]
  plug :require_guest_user when action in [:login, :register]

  @spec index(Plug.Conn.t(), any) :: Plug.Conn.t()
  def index(conn, _) do
    render(conn, "index.json", user: conn.assigns[:current_user])
  end

  @spec register(Plug.Conn.t(), map) :: Plug.Conn.t()
  def register(conn, %{"user" => params}) do
    with {:ok, user} <- Users.register_user(params),
         {:ok, _transaction} <- Accounts.give_new_user_bonus(user) do
      conn
      |> put_status(201)
      |> render("register.json", user: user)
    end
  end

  @spec login(Plug.Conn.t(), map) :: Plug.Conn.t()
  def login(conn, %{"email" => email, "password" => password}) do
    if user = Users.get_user_by_email_and_password(email, password) do
      if user.is_active do
        token = get_token(user)
        render(conn, "login.json", user: user, token: token)
      else
        {:error, :bad_request, "Your account is not active."}
      end
    else
      {:error, :bad_request, "Invalid username or password."}
    end
  end

  @spec logout(Plug.Conn.t(), any) :: Plug.Conn.t()
  def logout(conn, _) do
    conn.assigns[:current_user]
    |> get_token()
    |> delete_token()

    conn
    |> put_status(200)
    |> json(%{ok: true})
  end
end
