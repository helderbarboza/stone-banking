defmodule StoneWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use StoneWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(StoneWeb.ChangesetView)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(StoneWeb.ErrorView)
    |> render(:"404")
  end

  def call(conn, {:error, :bad_request, err}) do
    conn
    |> put_status(:bad_request)
    |> put_view(StoneWeb.ErrorView)
    |> render(:"400", error: err)
  end

  def call(conn, {:error, :conflict, err}) do
    conn
    |> put_status(:conflict)
    |> put_view(StoneWeb.ErrorView)
    |> render(:"409", error: err)
  end

  def call(conn, {:error, :unprocessable_entity, err}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(StoneWeb.ErrorView)
    |> render(:"422", error: err)
  end
end
