defmodule StoneWeb.TransactionController do
  use StoneWeb, :controller

  import StoneWeb.Auth

  alias Stone.Accounts
  alias Stone.Users.UserNotifier
  alias StoneWeb.Transaction

  action_fallback StoneWeb.FallbackController

  plug :require_authenticated_user
  plug :require_staff_user when action in [:index, :show]

  def index(conn, %{"filter" => "year"} = params) do
    transactions = Accounts.list_transactions_by_year(params["year"])
    render(conn, "index.json", transactions: transactions)
  end

  def index(conn, %{"filter" => "month"} = params) do
    transactions = Accounts.list_transactions_by_month(params["year"], params["month"])
    render(conn, "index.json", transactions: transactions)
  end

  def index(conn, %{"filter" => "day"} = params) do
    transactions =
      Accounts.list_transactions_by_day(params["year"], params["month"], params["day"])

    render(conn, "index.json", transactions: transactions)
  end

  def index(conn, params) do
    if Map.get(params, "filter", "all") == "all" do
      transactions = Accounts.list_transactions()
      render(conn, "index.json", transactions: transactions)
    end
  end

  def show(conn, %{"id" => id}) do
    transaction = Accounts.get_transaction!(id)
    render(conn, "show.json", transaction: transaction)
  end

  def withdraw(conn, %{"amount" => amount}) when is_binary(amount) do
    user = conn.assigns[:current_user]

    with :ok <- Transaction.validate_funds(user, amount),
         {:ok, transaction} <- Accounts.create_withdraw(user, amount) do
      {:ok, _} = UserNotifier.deliver_transaction_notification(user, transaction)

      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.transaction_path(conn, :show, transaction))
      |> render("show.json", transaction: transaction)
    else
      {:error, :not_enough_funds} -> {:error, :conflict, "Insufficient funds."}
      {:error, %Ecto.Changeset{}} = changeset_error -> changeset_error
    end
  end

  def transfer(conn, %{"receiver_id" => receiver_id, "amount" => amount}) do
    payer = conn.assigns[:current_user]

    with :ok <- Transaction.validate_different_users(payer.id, receiver_id),
         :ok <- Transaction.validate_funds(payer, amount),
         {:ok, receiver} <- Transaction.validate_user(receiver_id),
         {:ok, trans_payer, _trans_receiver} <- Accounts.create_transfer(payer, receiver, amount) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.transaction_path(conn, :show, trans_payer))
      |> render("show.json", transaction: trans_payer)
    else
      {:error, :not_enough_funds} ->
        {:error, :conflict, "Insufficient funds."}

      {:error, :same_user} ->
        {:error, :unprocessable_entity, "Payer and receiver cannot be the same user."}

      {:error, :inactive_user} ->
        {:error, :conflict, "Receiver is not active."}

      {:error, :user_not_found} ->
        {:error, :conflict, "Receiver invalid."}

      {:error, %Ecto.Changeset{}} = changeset_error ->
        changeset_error
    end
  end

  def balance(conn, _) do
    user = conn.assigns[:current_user]
    balance = Accounts.user_balance(user)

    render(conn, "balance.json", balance: balance)
  end
end
