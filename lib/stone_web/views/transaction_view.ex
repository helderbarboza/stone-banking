defmodule StoneWeb.TransactionView do
  @moduledoc false

  use StoneWeb, :view
  alias StoneWeb.TransactionView

  def render("index.json", %{transactions: transactions}) do
    %{transactions: render_many(transactions, TransactionView, "transaction.json")}
  end

  def render("show.json", %{transaction: transaction}) do
    %{transaction: render_one(transaction, TransactionView, "transaction.json")}
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{
      id: transaction.id,
      amount: transaction.amount,
      description: transaction.description,
      type: transaction.type
    }
  end

  def render("balance.json", %{balance: balance}) do
    %{balance: balance}
  end
end
