defmodule StoneWeb.AuthView do
  use StoneWeb, :view
  alias StoneWeb.AuthView

  def render("index.json", %{user: user}) do
    %{data: render_one(user, AuthView, "privileged_user.json", as: :user)}
  end

  def render("register.json", %{:user => user}) do
    %{
      data: render_one(user, AuthView, "privileged_user.json", as: :user)
    }
  end

  def render("privileged_user.json", %{user: user}) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      full_name: user.full_name
    }
  end

  def render("login.json", %{:user => user, :token => token}) do
    %{
      data: %{
        user: render_one(user, AuthView, "privileged_user.json", as: :user)
      },
      token: token
    }
  end
end
