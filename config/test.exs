import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :stone, Stone.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "db",
  database: "stone_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :stone, StoneWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "rcX/0ZYxnUMWytCke2NufHuYEyuc4l5vCKBk6gCXYF/3ZrDIPRaz5aLqld9X2wVb",
  server: false

# In test we don't send emails.
config :stone, Stone.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# https://hexdocs.pm/bcrypt_elixir/Bcrypt.html
config :bcrypt_elixir, log_rounds: 4
