defmodule StoneWeb.AuthControllerTest do
  use StoneWeb.ConnCase, async: true

  import Stone.Factory
  alias Stone.Repo
  alias Stone.Users

  @correct_credentials %{email: "test0@test0.local", password: "test000"}
  @incorrect_credentials %{email: "incorrect", password: "incorrect"}

  def fixture(:user, params \\ []),
    do: insert(:user, Keyword.merge([email: "test0@test0.local", password: "test000"], params))

  def create_user(_), do: %{user: fixture(:user)}

  def create_register_params(_),
    do: %{
      register_params: %{
        user: Map.put(params_for(:user, %{}), :password, "Testing&1234")
      }
    }

  describe "register view" do
    setup [:create_register_params, :create_user]

    test "registers a new user", %{conn: conn, register_params: register_params} do
      conn =
        post(
          conn,
          Routes.auth_path(conn, :register),
          register_params
        )

      assert json_response(conn, 201)
      assert Users.get_user!(json_response(conn, 201)["data"]["id"])
    end

    test "allows only unauthenticated users", %{conn: conn, user: user} do
      token = Users.generate_user_session_token(user)

      conn =
        conn
        |> put_req_header("authorization", "Token #{token}")
        |> post(Routes.auth_path(conn, :register))

      assert json_response(conn, 401)
    end

    test "registers a new user and check his 'new user bonus'", %{
      conn: conn,
      register_params: register_params
    } do
      bonus = Decimal.new("1000.00")

      conn = post(conn, Routes.auth_path(conn, :register), register_params)
      assert json_response(conn, 201)
      user = %{id: json_response(conn, 201)["data"]["id"]}

      balance = Stone.Accounts.user_balance(user)
      assert Decimal.eq?(bonus, balance)
    end
  end

  describe "me view" do
    setup :create_user

    test "returns the privileged user", %{conn: conn, user: user} do
      token = Users.generate_user_session_token(user)

      conn =
        conn
        |> put_req_header("authorization", "Token #{token}")
        |> get(Routes.auth_path(conn, :index))

      assert json_response(conn, 200)
    end

    test "only allows logged in users", %{conn: conn} do
      conn = get(conn, Routes.auth_path(conn, :index))
      assert json_response(conn, 401)
    end
  end

  describe "login view" do
    setup :create_user

    test "returns a token on correct credentials", %{conn: conn} do
      conn = post(conn, Routes.auth_path(conn, :login), @correct_credentials)
      assert %{"token" => _token} = json_response(conn, 200)
    end

    test "fails on incorrect credentials", %{conn: conn} do
      conn = post(conn, Routes.auth_path(conn, :login), @incorrect_credentials)
      assert response(conn, 400)
    end

    test "rejects inactive users", %{conn: conn, user: user} do
      user
      |> Users.User.role_changeset(%{is_active: false})
      |> Repo.update!()

      conn = post(conn, Routes.auth_path(conn, :login), @correct_credentials)
      assert response(conn, 400)
    end
  end

  describe "logout view" do
    setup :create_user

    test "logs out the privileged user", %{conn: conn, user: user} do
      token = Users.generate_user_session_token(user)

      conn =
        conn
        |> put_req_header("authorization", "Token #{token}")
        |> post(Routes.auth_path(conn, :logout))

      assert json_response(conn, 200)
    end

    test "only allows logged in users", %{conn: conn} do
      conn = post(conn, Routes.auth_path(conn, :logout))
      assert json_response(conn, 401)
    end
  end
end
