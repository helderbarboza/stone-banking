defmodule StoneWeb.TransactionControllerTest do
  use StoneWeb.ConnCase

  import Stone.Factory
  import Swoosh.TestAssertions

  def fixture(name, params \\ [])
  def fixture(:user, params), do: insert(:user, Keyword.merge([password: "test000"], params))
  def fixture(:transaction, params), do: insert(:transaction, params)

  def create_user(_), do: %{user: fixture(:user)}
  def create_user_a(_), do: %{user_a: fixture(:user)}
  def create_user_b(_), do: %{user_b: fixture(:user)}
  def create_admin(_), do: %{admin: fixture(:user, is_staff: true)}
  def create_transaction(_), do: %{transaction: fixture(:transaction)}

  def create_transactions(_) do
    user = fixture(:user)

    params_list = [
      # day: 2021-01-01
      [inserted_at: ~N[2021-01-01 01:00:00], description: "by day"],
      [inserted_at: ~N[2021-01-01 02:00:00], description: "by day"],
      [inserted_at: ~N[2021-01-01 03:00:00], description: "by day"],
      # month: 2021-02
      [inserted_at: ~N[2021-02-01 00:00:00], description: "by month"],
      [inserted_at: ~N[2021-02-02 00:00:00], description: "by month"],
      [inserted_at: ~N[2021-02-03 00:00:00], description: "by month"],
      # year: 2022
      [inserted_at: ~N[2022-01-01 00:00:00], description: "by year"],
      [inserted_at: ~N[2022-01-02 00:00:00], description: "by year"]
    ]

    transactions =
      for param <- params_list do
        fixture(:transaction, [{:user, user} | param])
      end

    %{transactions: transactions}
  end

  describe "show" do
    setup [:create_admin, :create_transaction]

    test "retrieves one transaction", %{conn: conn, admin: admin, transaction: transaction} do
      conn =
        conn
        |> generate_authentication(admin)
        |> get(Routes.transaction_path(conn, :show, transaction.id))

      assert json_response(conn, 200)["transaction"]
      assert transaction.id == json_response(conn, 200)["transaction"]["id"]
    end
  end

  describe "index without filter" do
    setup [:create_admin, :create_transaction, :create_user]

    test "lists all transactions as admin", %{conn: conn, admin: admin, transaction: transaction} do
      conn =
        conn
        |> generate_authentication(admin)
        |> get(Routes.transaction_path(conn, :index))

      assert [resp_transaction] = json_response(conn, 200)["transactions"]
      assert transaction.amount == Decimal.new(resp_transaction["amount"])
      assert transaction.id == resp_transaction["id"]
      assert transaction.type == String.to_existing_atom(resp_transaction["type"])
    end

    test "renders error when user is not admin", %{conn: conn, user: user} do
      conn =
        conn
        |> generate_authentication(user)
        |> get(Routes.transaction_path(conn, :index))

      assert json_response(conn, 401)
    end
  end

  describe "index with filter" do
    setup [:create_admin, :create_transactions]

    test "by day", %{conn: conn, admin: admin} do
      path = Routes.transaction_path(conn, :index)

      conn =
        conn
        |> generate_authentication(admin)
        |> get(path, filter: "day", day: "01", month: "01", year: "2021")

      assert transactions = json_response(conn, 200)["transactions"]
      assert Enum.all?(transactions, &(&1["description"] == "by day"))
    end

    test "by month", %{conn: conn, admin: admin} do
      path = Routes.transaction_path(conn, :index)

      conn =
        conn
        |> generate_authentication(admin)
        |> get(path, filter: "month", month: "02", year: "2021")

      assert transactions = json_response(conn, 200)["transactions"]
      assert Enum.all?(transactions, &(&1["description"] == "by month"))
    end

    test "by year", %{conn: conn, admin: admin} do
      path = Routes.transaction_path(conn, :index)

      conn =
        conn
        |> generate_authentication(admin)
        |> get(path, filter: "year", year: "2022")

      assert transactions = json_response(conn, 200)["transactions"]
      assert Enum.all?(transactions, &(&1["description"] == "by year"))
    end
  end

  describe "create withdraw" do
    setup :create_user

    test "renders transaction when data is valid", %{conn: conn, user: user} do
      # inserts initial balance
      fixture(:transaction, user: user, amount: "15.00")

      amount = "10.00"

      conn =
        conn
        |> generate_authentication(user)
        |> post(Routes.transaction_path(conn, :withdraw), amount: amount)

      assert json_response(conn, 201)["transaction"]
      assert json_response(conn, 201)["transaction"]["amount"] == "-#{amount}"
      assert_email_sent(to: user.email)
    end

    test "renders errors when not enough balance", %{conn: conn, user: user} do
      # inserts initial balance
      fixture(:transaction, user: user, amount: "1.00")

      conn =
        conn
        |> generate_authentication(user)
        |> post(Routes.transaction_path(conn, :withdraw), amount: "5.00")

      assert "Insufficient funds." == json_response(conn, 409)["error"]
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn =
        conn
        |> generate_authentication(user)
        |> post(Routes.transaction_path(conn, :withdraw), amount: "-100")

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "create transfer" do
    setup [:create_user_a, :create_user_b]

    test "renders transaction when data is valid", %{conn: conn, user_a: payer, user_b: receiver} do
      # inserts initial balance
      fixture(:transaction, user: payer, amount: "15.00")
      amount = "10.00"

      conn =
        conn
        |> generate_authentication(payer)
        |> post(Routes.transaction_path(conn, :transfer),
          receiver_id: receiver.id,
          amount: amount
        )

      assert json_response(conn, 201)["transaction"]
      assert json_response(conn, 201)["transaction"]["amount"] == "-#{amount}"
    end

    test "renders errors when not enough balance", %{conn: conn, user_a: payer, user_b: receiver} do
      # inserts initial balance
      fixture(:transaction, user: payer, amount: "1.00")

      conn =
        conn
        |> generate_authentication(payer)
        |> post(Routes.transaction_path(conn, :transfer),
          receiver_id: receiver.id,
          amount: "5.00"
        )

      assert "Insufficient funds." == json_response(conn, 409)["error"]
    end

    test "renders errors when receiver is not active", %{conn: conn, user_a: payer} do
      receiver = fixture(:user, is_active: false)

      fixture(:transaction, user: payer, amount: "20.00")

      conn =
        conn
        |> generate_authentication(payer)
        |> post(Routes.transaction_path(conn, :transfer),
          receiver_id: receiver.id,
          amount: "10.00"
        )

      assert "Receiver is not active." == json_response(conn, 409)["error"]
    end

    test "renders errors when receiver user does not exist", %{conn: conn, user_a: payer} do
      fixture(:transaction, user: payer, amount: "45.00")

      conn =
        conn
        |> generate_authentication(payer)
        |> post(Routes.transaction_path(conn, :transfer), receiver_id: -1, amount: "10.00")

      assert "Receiver invalid." == json_response(conn, 409)["error"]
    end

    test "renders errors when trying to transfer to himself", %{conn: conn, user_a: user} do
      fixture(:transaction, user: user, amount: "45.00")

      conn =
        conn
        |> generate_authentication(user)
        |> post(Routes.transaction_path(conn, :transfer),
          receiver_id: user.id,
          amount: "10.00"
        )

      assert "Payer and receiver cannot be the same user." == json_response(conn, 422)["error"]
    end

    test "renders errors when data is invalid", %{conn: conn, user_a: payer, user_b: receiver} do
      conn =
        conn
        |> generate_authentication(payer)
        |> post(Routes.transaction_path(conn, :transfer),
          receiver_id: receiver.id,
          amount: "0"
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "balance" do
    setup :create_user

    test "renders balance", %{conn: conn, user: user} do
      transaction = fixture(:transaction, user: user, amount: amount = "23.00")

      conn =
        conn
        |> generate_authentication(user)
        |> get(Routes.transaction_path(conn, :balance))

      assert json_response(conn, 200)["balance"] == amount
    end
  end

  defp generate_authentication(conn, user) do
    token = Stone.Users.generate_user_session_token(user)
    put_req_header(conn, "authorization", "Token #{token}")
  end
end
