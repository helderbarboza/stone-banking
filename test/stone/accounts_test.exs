defmodule Stone.AccountsTest do
  use Stone.DataCase

  import Stone.Factory

  alias Stone.Accounts
  alias Stone.Accounts.Transaction
  alias Stone.Repo

  def fixture(:transaction), do: insert(:transaction)
  def fixture(:user), do: insert(:user)
  def create_transaction(_), do: %{transaction: fixture(:transaction)}
  def create_user(_), do: %{user: fixture(:user)}

  describe "transactions" do
    @invalid_attrs %{amount: nil, description: nil, type: nil}

    setup :create_transaction

    test "list_transactions/0 returns all transactions", %{transaction: transaction} do
      assert Repo.preload(Accounts.list_transactions(), :user) == [transaction]
    end

    test "get_transaction!/1 returns the transaction with given id", %{transaction: transaction} do
      assert transaction.id |> Accounts.get_transaction!() |> Repo.preload(:user) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction" do
      params = %{
        amount: "-52.5",
        description: "refund #123",
        type: "payment"
      }

      valid_attrs = params_with_assocs(:transaction, params)

      assert {:ok, %Transaction{} = transaction} = Accounts.create_transaction(valid_attrs)
      assert transaction.amount == Decimal.new(params.amount)
      assert transaction.description == params.description
      assert transaction.type == String.to_existing_atom(params.type)
    end

    test "create_transaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_transaction(@invalid_attrs)
    end

    test "update_transaction/2 with valid data updates the transaction", %{
      transaction: transaction
    } do
      update_attrs = %{
        amount: "-456.7",
        description: "some updated description",
        type: "withdraw"
      }

      assert {:ok, %Transaction{} = transaction} =
               Accounts.update_transaction(transaction, update_attrs)

      assert transaction.amount == Decimal.new(update_attrs.amount)
      assert transaction.description == update_attrs.description
      assert transaction.type == String.to_existing_atom(update_attrs.type)
    end

    test "update_transaction/2 with invalid data returns error changeset", %{
      transaction: transaction
    } do
      assert {:error, %Ecto.Changeset{}} =
               Accounts.update_transaction(transaction, @invalid_attrs)

      assert Repo.reload(transaction) == Accounts.get_transaction!(transaction.id)
    end

    test "delete_transaction/1 deletes the transaction", %{transaction: transaction} do
      assert {:ok, %Transaction{}} = Accounts.delete_transaction(transaction)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_transaction!(transaction.id) end
    end
  end

  describe "new user's bonus" do
    setup :create_user

    test "valid", %{user: user} do
      assert {:ok, %Transaction{} = transaction} = Accounts.give_new_user_bonus(user)
      assert transaction.amount == Decimal.new("1000")
    end

    test "invalid user" do
      assert {:error, %Ecto.Changeset{}} = Accounts.give_new_user_bonus(%{id: -1})
    end
  end
end
