defmodule Stone.Factory do
  @moduledoc "Ecto's factories."

  use ExMachina.Ecto, repo: Stone.Repo

  def user_factory(attrs) do
    password = Map.get(attrs, :password, "password")

    user = %Stone.Users.User{
      is_active: true,
      is_staff: false,
      full_name: "John Doe",
      username: sequence("user"),
      email: sequence(:email, &"email-#{&1}@example.com"),
      hashed_password: Bcrypt.hash_pwd_salt(password)
    }

    merge_attributes(user, attrs)
  end

  def transaction_factory(attrs) do
    transaction = %Stone.Accounts.Transaction{
      description: "a deposit",
      type: "deposit",
      amount: "150.25",
      user: build(:user)
    }

    merge_attributes(transaction, attrs)
  end
end
